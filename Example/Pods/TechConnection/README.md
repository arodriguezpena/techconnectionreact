# TechConnection

[![CI Status](https://img.shields.io/travis/arodriguezp2003/TechConnection.svg?style=flat)](https://travis-ci.org/arodriguezp2003/TechConnection)
[![Version](https://img.shields.io/cocoapods/v/TechConnection.svg?style=flat)](https://cocoapods.org/pods/TechConnection)
[![License](https://img.shields.io/cocoapods/l/TechConnection.svg?style=flat)](https://cocoapods.org/pods/TechConnection)
[![Platform](https://img.shields.io/cocoapods/p/TechConnection.svg?style=flat)](https://cocoapods.org/pods/TechConnection)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TechConnection is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TechConnection'
```

## Author

arodriguezp2003, alejandrorodriguezpena@gmail.com

## License

TechConnection is available under the MIT license. See the LICENSE file for more info.
