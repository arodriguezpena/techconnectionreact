//
//  Factory.swift
//  Pods-TechConnection_Example
//
//  Created by Alejandro  Rodriguez on 8/18/19.
//

import Foundation

@objc(TCFactory)
public class TCFactory: NSObject {
   @objc public func getModel() -> TCModelLogic {
        let service = TCService()
        let model =  TCModel()
        model.service = service
        return model
    }
}
