//
//  Model.swift
//  Pods-TechConnection_Example
//
//  Created by Alejandro  Rodriguez on 8/18/19.
//

import Foundation

@objc public  protocol TCModelLogic {
    func login(email: String,  password: String,  callback: @escaping (_ result: IResponse)->())
    func getUsers(delay: Int, callback: @escaping (_ result: IResponse)->())
}

/// Model de conexion de prueba
@objcMembers public class TCModel:NSObject, TCModelLogic {
    var service : TCServiceLogic?
    
    @objc override init() {}
    
    /// Login de usuario
    ///
    /// - Parameters:
    ///   - email: email o username
    ///   - password:
    ///   - callback: responde un objeto tipo IResponse.
    @objc public func login(email: String,password: String,callback: @escaping (IResponse) -> ()) {
        if email == "" || password == "" {
            callback(IResponse(success: false, message: "Ingrese email y password", data: nil))
            return
        }
        service?.login(email: email, password: password, callback: callback)
    }
    

    /// Listado de usuarios
    ///
    /// - Parameters:
    ///   - delay: tiempo de espera para entregar el response del servicio
    ///   - callback: IResponse
    @objc public func getUsers(delay: Int , callback: @escaping (IResponse) -> ()) {
        service?.getUsers(delay: delay, callback: callback)
    }
}
