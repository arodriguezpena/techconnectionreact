//
//  Service.swift
//  Pods-TechConnection_Example
//
//  Created by Alejandro  Rodriguez on 8/18/19.
//

import Foundation

protocol TCServiceLogic {
    func login(email: String, password: String, callback: @escaping (_ result: IResponse)->())
    func getUsers(delay: Int, callback: @escaping (_ result: IResponse)->())
}

class TCService: TCServiceLogic {
 
    func login(email: String, password: String, callback: @escaping (_ result: IResponse)->()){
        let url = URL(string: "\(CONFIG.URL.toString())/api/login" )!
        var request = URLRequest(url: url)

        request.httpMethod = "POST"
        request.httpBody = "email=\(email)&password=\(password)".data(using: .utf8)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data, error == nil else {
                    print(error! as NSError)
                    let res = IResponse(success: false, message: "Error en el servidor", data: nil)
                    callback(res)
                    return
                }
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode == 400 {
                        
                        do {
                            let jsonDic = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
                            let error = jsonDic!["error"]
                            let res = IResponse(success: false, message: (error as! String), data: nil)
                            callback(res)
                            return
                        } catch {
                            let res = IResponse(success: false, message: "Error en el servidor", data: nil)
                            callback(res)
                        }
                       
                    }
                }
                do {
                    let jsonDic = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    
                    if let dic = jsonDic as? NSDictionary {
                        let token = ["token":  dic["token"]]
                        let res = IResponse(success: true, message: "", data: token)
                        callback(res)
                    } else {
                        let res = IResponse(success: false, message: "Error en el servidor", data: nil)
                        callback(res)
                    }
                } catch {
                    let res = IResponse(success: false, message: "Error en el servidor", data: nil)
                    callback(res)
                    print(error as NSError)
                }
            }
        }
        task.resume()
    }
    
    
    func getUsers(delay:Int, callback: @escaping (_ result: IResponse)->()) {
        let url = URL(string: "\(CONFIG.URL.toString())/api/users?delay=\(delay)" )!
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data, error == nil else {
                    print(error! as NSError)
                    let res = IResponse(success: false, message: "Error en el servidor", data: nil)
                    callback(res)
                    return
                }
                
                do {
                    let jsonDic = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    
                    if let dic = jsonDic as? NSDictionary {
                        let res = IResponse(success: true, message: "", data: dic["data"])
                        callback(res)
                    } else {
                        let res = IResponse(success: false, message: "Error en el servidor", data: nil)
                        callback(res)
                    }
                } catch {
                    let res = IResponse(success: false, message: "Error en el servidor", data: nil)
                    callback(res)
                    print(error as NSError)
                }
            }
        }
        task.resume()
    }
}
