# TechConnectionReact

[![CI Status](https://img.shields.io/travis/arodriguezp2003/TechConnectionReact.svg?style=flat)](https://travis-ci.org/arodriguezp2003/TechConnectionReact)
[![Version](https://img.shields.io/cocoapods/v/TechConnectionReact.svg?style=flat)](https://cocoapods.org/pods/TechConnectionReact)
[![License](https://img.shields.io/cocoapods/l/TechConnectionReact.svg?style=flat)](https://cocoapods.org/pods/TechConnectionReact)
[![Platform](https://img.shields.io/cocoapods/p/TechConnectionReact.svg?style=flat)](https://cocoapods.org/pods/TechConnectionReact)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TechConnectionReact is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TechConnectionReact'
```

## Author

arodriguezp2003, alejandrorodriguezpena@gmail.com

## License

TechConnectionReact is available under the MIT license. See the LICENSE file for more info.
