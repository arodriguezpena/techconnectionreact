//
//  TechLib.h
//  Pods
//
//  Created by Alejandro  Rodriguez on 8/6/19.
//

#if __has_include("RCTBridgeModule.h")
#import "RCTBridgeModule.h"
#else
#import <React/RCTBridgeModule.h>
#endif
NS_ASSUME_NONNULL_BEGIN

@interface TechConnectionReact : NSObject <RCTBridgeModule>
    
    @end
NS_ASSUME_NONNULL_END

