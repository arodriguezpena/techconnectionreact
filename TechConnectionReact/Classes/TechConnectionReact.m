//
//  TechLib.m
//  Pods-TechLib_Example
//
//  Created by Alejandro  Rodriguez on 8/6/19.
//


#import "RCTConvert.h"
#import "TechConnectionReact.h"
@import TechConnection;
@implementation TechConnectionReact : NSObject
    
- (dispatch_queue_t)methodQueue
    {
        return dispatch_get_main_queue();
    }
    RCT_EXPORT_MODULE();
    RCT_REMAP_METHOD(login,
                     email:(NSString *)email
                     password:(NSString *)password
                     getWithResolver:(RCTPromiseResolveBlock)resolve
                     rejecter:(RCTPromiseRejectBlock)reject) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            TCModel * model = [[[TCFactory alloc] init] getModel];
            [model loginWithEmail: email password:password callback:^(IResponse * _Nonnull res) {
                if (res.success) {
                    NSDictionary *dic = @{@"success": @(res.success), @"data": res.data};
                    resolve(dic);
                } else {
                    NSDictionary *dic = @{@"success": @(res.success), @"message": res.message};
                    resolve(dic);
                }
            }];
        });
    }
    RCT_REMAP_METHOD(getUsers,
                     delay:(NSInteger)delay
                     getWithResolver:(RCTPromiseResolveBlock)resolve
                     rejecter:(RCTPromiseRejectBlock)reject) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            TCModel * model = [[[TCFactory alloc] init] getModel];
            [model getUsersWithDelay:delay callback:^(IResponse * _Nonnull res) {
                NSDictionary *dic = @{@"success": @(res.success), @"message": res.message, @"data": res.data};
                resolve(dic);
            }];
        });
    }
@end




